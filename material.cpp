
#include <iostream>
#include <cstdlib>

#include "material.h"
#include "rgb.h"

using namespace std;

Material::Material(void)
{
}

Material::Material(string newName):
   m_name(newName)
{
}

Material::~Material(void)
{
}

string Material::GetName(void)
{
   return m_name;
}

void Material::GetAmbient(drgbType* pPixel)
{
   *pPixel = m_ambient;
}

void Material::GetDiffuse(drgbType* pPixel)
{
   *pPixel = m_diffuse;
}

void Material::GetSpecular(drgbType* pPixel)
{
   *pPixel = m_specular;
}

void Material::Parse(void)
{
   string token;
   cin >> m_name;
   cin.ignore(100,'\n');
   cin.ignore(100,'\n');
   cin >> token;

   while(token != "}") {
      char letter = token[0];

      switch(letter)
      {
         case 'a':
            cin >> m_ambient.r >> m_ambient.g >> m_ambient.b;
            break;
         case 'd':
            cin >> m_diffuse.r >> m_diffuse.g >> m_diffuse.b;
            break;
         case 's':
            cin >> m_specular.r >> m_specular.g >> m_specular.b;
            break;
         default:
            cerr << token << " is not a valid property of " << m_name << endl;
            exit(1);
      }
      cin.ignore(100,'\n');
      cin >> token;
   }
}

void Material::Print(void)
{
   cout << "Type: Material" << endl;
   cout << "Name: " << m_name << endl;
   cout << "Ambient: " << m_ambient << endl;
   cout << "Diffuse: " << m_diffuse << endl;
   cout << "Specular: " << m_specular << endl;
}
