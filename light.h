
#ifndef LIGHT
#define LIGHT

using namespace std;

class Vector;
class Model;
class Object;
class drgbType;

class Light
{
protected:
   string m_name;
   string m_type;
   Vector* m_pLocation;
   Vector* m_pEmissivity;

public:
   Light();
   ~Light();

   Vector* GetLocation(void);
   Vector* GetEmissivity(void);

   // The Parse function parses the location and emissivity values from stdin.
   void Parse(void);

   // The Print function prints information about the light object to stdout.
   // This includes the name, type, location, and emissivity values.
   void Print(void);

   // The Illuminate function illuminates the HitObject with the current light
   // adding diffuse lighting to the scene.
   //    pModel - Pointer to the model that is being illuminated.
   //    pHitObject - Pointer to the object that was hit and is currently being
   //       illuminated.
   //    pPixel - Pointer to the pixel to add the diffuse lighting to.
   void Illuminate(Model* pModel, Object* pHitObject, drgbType* pPixel);
};

#endif //LIGHT
