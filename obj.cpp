
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>

#include "obj.h"
#include "vector.h"

using namespace std;

Obj::Obj(void):
   Object("obj"),
   m_pLastHitTriangle(NULL)
{
}

Obj::~Obj(void)
{
   //Delete all triangle objects from list
   for(list<Triangle*>::iterator it=m_triangles.begin(); it!=m_triangles.end(); it++)
      delete *it;
   for(vector<Vector*>::iterator it=m_vertices.begin(); it!=m_vertices.end(); it++)
      delete *it;
   for(vector<Vector*>::iterator it=m_textureCoords.begin(); it!=m_textureCoords.end(); it++)
      delete *it;
   for(vector<Vector*>::iterator it=m_normals.begin(); it!=m_normals.end(); it++)
      delete *it;
}

void Obj::Parse(void)
{
   string token,junk;
   cin >> m_name;

   cin >> token;
   while(token != "}") {
      if(token == "filename")
         cin >> m_objFile;
      cin >> token;
   }

   //Set the material for the entire obj as "white." This material must be
   //specified in the scene file with the name "white."
   m_mat = "white";
}

void Obj::Init(Model* pModel)
{
   Object::Init(pModel);

   ParseObj(pModel);

   //Iterate through all of the triangles and assign the same material as the
   //obj object. This is so everything will render as the material specified
   //by m_mat
   for(list<Triangle*>::iterator it=m_triangles.begin(); it!=m_triangles.end(); it++)
      (*it)->SetMaterial(m_pMaterial);
}

double Obj::Hits(Vector* pBase, Vector* pDirection)
{
   double distance = -1.0;
   double minDistance = INT_MAX;
   m_pLastHitTriangle = NULL;
   for(list<Triangle*>::iterator it=m_triangles.begin(); it!=m_triangles.end(); it++) {
      distance = (*it)->Hits(pBase,pDirection);
      if(distance >= 0.0 && distance < minDistance) {
         minDistance = distance;
         m_pLastHitTriangle = *it;
      }
   }
   if(m_pLastHitTriangle != NULL) {
      //Grab the last hit location and normal as lighting calculations will need these
      *m_pHitLoc = *(m_pLastHitTriangle->GetHitLoc());
      *m_pNormal = *(m_pLastHitTriangle->GetNormal());
      return minDistance;
   }
   return -1.0;
}

void Obj::Print(void)
{
   cout << "Name: " << m_name << endl;
   cout << "Vertices: " << endl;
   for(vector<Vector*>::iterator it=m_vertices.begin(); it!=m_vertices.end(); it++)
      cout << *(*it) << endl;
   cout << "Vertices Count: " << m_vertices.size() << endl;
   cout << "Texture Coordinates: " << endl;
   for(vector<Vector*>::iterator it=m_textureCoords.begin(); it!=m_textureCoords.end(); it++)
      cout << *(*it) << endl;
   cout << "Texture Coordinates Count: " << m_textureCoords.size() << endl;
   cout << "Normals: " << endl;
   for(vector<Vector*>::iterator it=m_normals.begin(); it!=m_normals.end(); it++)
      cout << *(*it) << endl;
   cout << "Normals Count: " << m_normals.size() << endl;
   for(list<Triangle*>::iterator it=m_triangles.begin(); it!=m_triangles.end(); it++)
      (*it)->Print();
   cout << "Triangles Count: " << m_triangles.size() << endl;
}

void Obj::ParseObj(Model* pModel)
{
   char token,trash,temp;
   bool loadedMaterialFile = false;
   string id;

   //Material *curMaterial;
   fstream file;

   //Used to offset the obj vertices in the specialized section
   Vector offset(0.3,0,-1.0);
   double tempVal;

   file.open(m_objFile.c_str(),fstream::in);
   if(file.fail()) {
      cerr << "Error: Could not open file " << m_objFile << endl;
      exit(1);
   }

   cout << "Opened " << m_objFile << endl;
   cout << "Parsing " << endl;

   Vector *pVertex;
   while(file.get(token)) {
      switch(token) {
         case '#': //Comment in file so ignore line
            file.ignore(256,'\n');
            break;

         case 'm':
            file.putback(token);
            file >> id;
            if(id == "mtllib" && !loadedMaterialFile) { //Load material library if not already loaded
               file >> id;
               m_mtlFile = id;
               //parseMtl(pModel);
               loadedMaterialFile = true;
               file.ignore(256,'\n');
            }
            break;

         case 'v':
            //Load material file if not already loaded. In this case the
            //"mtllib" token was not at the beginning of the file, so try
            //to find a .mtl file in the same directory with the same name
            if(!loadedMaterialFile) {
               m_mtlFile = m_objFile;
               m_mtlFile.replace(m_mtlFile.length()-4,4,".mtl");
               ParseMtl(pModel);
               loadedMaterialFile = true;
            }
            file.get(temp);
            switch(temp) {
               case ' ': //Vertices
                  pVertex = new Vector();
                  file >> *pVertex;

                  //Switch y and z values since the tree obj file is on its side.
                  //Also scale by 0.7 and translate the tree so it is in the
                  //middle of the scene. This is specific for the EU55 obj file
                  //and should be removed to process any other obj file.
                  tempVal = pVertex->y;
                  pVertex->y = pVertex->z;
                  pVertex->z = tempVal;
                  *pVertex = 0.7**pVertex;
                  *pVertex = *pVertex+offset;
                  //======================================End Specialized Section

                  m_vertices.push_back(pVertex);
                  file.get(trash);
                  break;
               case 'n': //Normals
                  pVertex = new Vector();
                  file >> *pVertex;
                  m_normals.push_back(pVertex);
                  file.get(trash);
                  break;
               case 't': //Texture Coordinates - Assumes 2D coordinates currently
                  pVertex = new Vector();
                  file >> pVertex->x >> pVertex->y;
                  m_textureCoords.push_back(pVertex);
                  file.get(trash);
                  break;
            }
            break;

         case 'u':
            file.putback(token);
            file >> id;
            if(id == "usemtl") { //Use a material
               file >> id;
               file.ignore(256,'\n');
               //Lookup the material to be used. Every surface will be given
               //this material until the next usemtl is hit
               //curMaterial = object->materialLookup(id);
            }
            break;

         case 'f':
            file.get(temp);
            if(temp == ' ' && isdigit(file.peek())) { //Build a surface
               int vert,tex,norm,slash=0;
               vector<Vector*> trianglePoints;
               //Read to end of line picking up each point for the triangle
               while(file.peek() != '\n' && file.peek() != EOF) {
                  temp = file.peek();
                  if(isdigit(temp)) {
                     //Pick up the vertex, texture coord, and normal
                     if(temp == '-') {
                        file.get(trash);
                        temp = file.peek();
                     }
                     switch(slash) {
                        case 0: //Vertice index
                           file >> vert;
                           if(file.peek() != '/') {
                           }
                           break;

                        case 1: //Texture index
                           file >> tex;
                           if(file.peek() != '/') {
                           }
                           break;

                        case 2: //Normal index
                           file >> norm;
                           if(file.peek() != '/') {
                              trianglePoints.push_back(m_vertices[vert-1]);
                           }
                           break;
                     }
                  } else if(temp == '/') {
                     slash++;
                     file.get(trash); //Pick up slash
                  } else if(temp == ' ') {
                     slash=0;
                     file.get(trash); //Pick up space
                  } else {
                     string token(1,temp);
                     ErrorAndExit(m_objFile,token);
                  }
               }
               if(trianglePoints.size() != 3) {
                  cerr << "Error: Incorrect number of verticies for triangle: " << trianglePoints.size() << endl;
                  exit(1);
               }

               //Create triangle object with the three verticies
               Triangle* pTriangle = new Triangle(trianglePoints[0],trianglePoints[1],trianglePoints[2]);
               m_triangles.push_back(pTriangle);
               trianglePoints.clear();
            }
            break;
      }
   }

   file.close();
   cout << "Closed " << m_objFile << endl;

}

void Obj::ParseMtl(Model* pModel)
{
}

void Obj::ErrorAndExit(string fileName, string token)
{
   cerr << "Error: Unexpected token in " << fileName << ": " << token << endl;
   exit(1);
}

void Obj::GetAmbient(drgbType* pPixel)
{
   m_pLastHitTriangle->GetAmbient(pPixel);
}

void Obj::GetDiffuse(drgbType* pPixel)
{
   m_pLastHitTriangle->GetDiffuse(pPixel);
}

void Obj::GetSpecular(drgbType* pPixel)
{
   m_pLastHitTriangle->GetSpecular(pPixel);
}
