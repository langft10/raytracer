
// Name: Thomas Langford
// Date: 09/16/13
// Class: csc4360
// Location: ~/csc4360/raytracing/raytracing/unitTests/raytracerTest.cpp
//
// This test file tests the list and vector classes.

#include <list>
#include <cmath>

#include "gtest/gtest.h"
#include "../vector.h"

using namespace std;

class ListTest : public testing::Test
{
protected:
   list<void*>* voidList;

   virtual void SetUp(void)
   {
      voidList = NULL;
      voidList = new list<void*>;
      ASSERT_TRUE(voidList != NULL);
   }

   virtual void TearDown(void)
   {
      for(list<void*>::iterator it=voidList->begin(); it!=voidList->end(); it++) {
         delete static_cast<int*>(*it);
      }
      delete voidList;
   }
};

class VectorTest: public testing::Test
{
protected:
   Vector* vectorA;
   Vector* vectorB;

   virtual void SetUp(void)
   {
      vectorA = vectorB = NULL;
      vectorA = new Vector(1,2,3);
      ASSERT_TRUE(vectorA != NULL);
      vectorB = new Vector(3,1,2);
      ASSERT_TRUE(vectorB != NULL);
   }

   virtual void TearDown(void)
   {
      delete vectorA;
      delete vectorB;
   }
};

TEST_F(ListTest, DefaultConstructor)
{
   ASSERT_TRUE(voidList != NULL);
   EXPECT_EQ(0,voidList->size());
}

TEST_F(ListTest, AddingOneElementToList)
{
   int* one = new int(1);
   voidList->push_back(static_cast<void*>(one));
   EXPECT_EQ(1,voidList->size());
   EXPECT_EQ(1,*static_cast<int*>(voidList->front()));
}

TEST_F(ListTest, AddingMultipleElements)
{
   for(int i=0; i<5; i++) {
      int* ptr = new int(i);
      voidList->push_back(static_cast<void*>(ptr));
      ASSERT_TRUE(voidList->back() != NULL);
      EXPECT_EQ(i,*static_cast<int*>(voidList->back()));
      EXPECT_EQ(i+1,voidList->size());
   }
}

TEST_F(ListTest, IteratingList)
{
   for(int i=0; i<5; i++) {
      int* ptr = new int(i);
      voidList->push_back(static_cast<void*>(ptr));
      ASSERT_TRUE(voidList->back() != NULL);
      EXPECT_EQ(i,*static_cast<int*>(voidList->back()));
      EXPECT_EQ(i+1,voidList->size());
   }

   int i=0;
   for(list<void*>::iterator it=voidList->begin(); it!=voidList->end(); it++,i++) {
      ASSERT_TRUE(*it != NULL);
      EXPECT_EQ(i,*static_cast<int*>(*it));
   }
}

//Copy constructor
TEST_F(VectorTest, CopyConstructor)
{
   Vector c(*vectorA);
   EXPECT_EQ(1,c.x);
   EXPECT_EQ(2,c.y);
   EXPECT_EQ(3,c.z);
}

//Overloaded = sign
TEST_F(VectorTest, Copy)
{
   Vector c = *vectorA;
   EXPECT_EQ(1,c.x);
   EXPECT_EQ(2,c.y);
   EXPECT_EQ(3,c.z);
}

//Overloaded + sign
TEST_F(VectorTest, Sum)
{
   Vector c = *vectorA+*vectorB;
   EXPECT_EQ(4,c.x);
   EXPECT_EQ(3,c.y);
   EXPECT_EQ(5,c.z);
}

//Overloaded * sign
TEST_F(VectorTest, DotProduct)
{
   EXPECT_EQ(11,*vectorA**vectorB);
}

//Overloaded * sign
TEST_F(VectorTest, Scale)
{
   Vector c = *vectorA*3.0;
   EXPECT_EQ(3,c.x);
   EXPECT_EQ(6,c.y);
   EXPECT_EQ(9,c.z);

   //Test with scale on left side
   c = 3.0**vectorA;
   EXPECT_EQ(3,c.x);
   EXPECT_EQ(6,c.y);
   EXPECT_EQ(9,c.z);
}

TEST_F(VectorTest, Length)
{
   double ans = sqrt(vectorA->x*vectorA->x+vectorA->y*vectorA->y+vectorA->z*vectorA->z);
   EXPECT_EQ(ans,vectorA->Length());
}

TEST_F(VectorTest, UnitVector)
{
   double scale = 1.0/sqrt(vectorA->x*vectorA->x+vectorA->y*vectorA->y+vectorA->z*vectorA->z);
   double i = vectorA->x*scale;
   double j = vectorA->y*scale;
   double k = vectorA->z*scale;
   Vector c = vectorA->UnitVector();

   EXPECT_EQ(i,c.x);
   EXPECT_EQ(j,c.y);
   EXPECT_EQ(k,c.z);
}

//Overloaded <<
TEST_F(VectorTest, Print)
{
   stringstream stream;
   stream << *vectorA;
   EXPECT_EQ("<1,2,3>",stream.str());
}

TEST_F(VectorTest, Projection)
{
   Vector c = vectorA->Projection(*vectorB);
   EXPECT_EQ(-32,c.x);
   EXPECT_EQ(-9,c.y);
   EXPECT_EQ(-19,c.z);
}

TEST_F(VectorTest, CrossProduct)
{
   Vector c = vectorA->CrossProduct(*vectorB);
   EXPECT_EQ(1,c.x);
   EXPECT_EQ(7,c.y);
   EXPECT_EQ(-5,c.z);
}
