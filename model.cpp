
#include <iostream>

#include "model.h"
#include "camera.h"
#include "material.h"
#include "object.h"
#include "light.h"

using namespace std;

Model::Model() 
{
}

Model::~Model(void)
{
   for(list<Object*>::iterator it=m_objs.begin(); it!=m_objs.end(); it++)
      delete *it;
   for(list<Material*>::iterator it=m_mats.begin(); it!=m_mats.end(); it++)
      delete *it;
   for(list<Light*>::iterator it=m_lights.begin(); it!=m_lights.end(); it++)
      delete *it;
   delete m_pCam;
}

void Model::Print(void)
{
   m_pCam->Print();
   cout << endl;
   for(list<Material*>::iterator it=m_mats.begin(); it!=m_mats.end(); it++) {
      (*it)->Print();
      cout << endl;
   }
   cout << endl;
   for(list<Object*>::iterator it=m_objs.begin(); it!=m_objs.end(); it++) {
      (*it)->Print();
      cout << endl;
   }
   cout << endl;
   for(list<Light*>::iterator it=m_lights.begin(); it!=m_lights.end(); it++) {
      (*it)->Print();
      cout << endl;
   }
   cout << endl;
}

void Model::Init(void)
{
   for(list<Object*>::iterator it = m_objs.begin(); it != m_objs.end(); it++)
      (*it)->Init(this);
}

Object* Model::FindClosestObject(Vector* pBase, 
                                 Vector* pDirection, 
                                 double& minDistance, 
                                 Object* pLastHit)
{
   Object* closest = NULL;
   //Find the closest object
   list<Object*>::iterator current = m_objs.begin();
   while(current != m_objs.end()) {
      if(*current != pLastHit) { //Exclude object if it was last hit
         double dist = (*current)->Hits(pBase,pDirection);
         if(dist >= 0.0 && (closest == NULL || dist < minDistance)) {
            closest = (*current);
            minDistance = dist;
         }
      }
      current++;
   }
   return closest;
}
