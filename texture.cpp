
#include <iostream>
#include <fstream>
#include <tiffio.h>

#include "texture.h"
#include "rgb.h"

using namespace std;

Texture::Texture(string textureFileName, double pixelXSize, double pixelYSize):
   m_pixelXSize(pixelXSize),
   m_pixelYSize(pixelYSize),
   m_imageBuffer(NULL),
   m_alphaValues(NULL)
{
   TIFF* pTiff = TIFFOpen(textureFileName.c_str(), "r");
   if(!pTiff) {
      cerr << "Error: Could not open texture file " << textureFileName << endl;
   }

   uint32* pRaster = NULL;
   TIFFGetField(pTiff, TIFFTAG_IMAGEWIDTH, &m_xDimension);
   TIFFGetField(pTiff, TIFFTAG_IMAGELENGTH, &m_yDimension);

   int nPixels = m_xDimension*m_yDimension;
   pRaster = static_cast<uint32*>(_TIFFmalloc(nPixels*sizeof(uint32)));
   if(!pRaster) {
      cerr << "Error: Could not malloc memory for texture." << endl;
   }
   m_alphaValues = new unsigned char [nPixels];
   if(!m_alphaValues) {
      cerr << "Error: Could not malloc memory for texture alpha values" << endl;
   }

   if(!TIFFReadRGBAImage(pTiff, m_xDimension, m_yDimension, pRaster, 0)) {
      cerr << "Error: Could not read texture file " << textureFileName << endl;
   }

   m_imageBuffer = new irgbType* [nPixels];
   for(int j=0,k=m_yDimension-1; j<m_yDimension && k>=0; j++,k--) {
      for(int i=0; i<m_xDimension; i++) {
         int offset1 = j*m_xDimension+i;
         int offset2 = k*m_xDimension+i;
         m_imageBuffer[offset2] = new irgbType(pRaster[offset1],pRaster[offset1]>>8,pRaster[offset1]>>16);
         m_alphaValues[offset2] = static_cast<unsigned char>(pRaster[offset1]>>24);
      }
   }

   _TIFFfree(pRaster);
   TIFFClose(pTiff);
}

Texture::~Texture(void)
{
   for(int i=0; i<m_xDimension*m_yDimension; i++)
      delete m_imageBuffer[i];
   delete [] m_imageBuffer;
   delete [] m_alphaValues;
}

void Texture::GetAlpha(int relativeX, int relativeY, double& alpha)
{
   int offset = (m_yDimension-relativeY-1)*m_xDimension+relativeX;
   alpha = m_alphaValues[offset];
}

void Texture::GetAlphaFit(double relativeX, double relativeY, double& alpha)
{
   GetAlpha(static_cast<int>(relativeX*m_xDimension),
            static_cast<int>(relativeY*m_yDimension),
            alpha);
}

void Texture::GetAlphaTile(double worldX, double worldY, double& alpha)
{
   //The location in pixel coordinates
   int pixelX = static_cast<int>(worldX/m_pixelXSize);
   int pixelY = static_cast<int>(worldY/m_pixelYSize);

   //Tile modded by the dimension
   int texelX = pixelX%m_xDimension;
   int texelY = pixelY%m_yDimension;

   //Get the alpha value
   GetAlpha(texelX,texelY,alpha);
}

void Texture::GetTexel(int relativeX, int relativeY, drgbType* pTexel)
{
   int offset = (m_yDimension-relativeY-1)*m_xDimension+relativeX;
   pTexel->r = static_cast<double>(m_imageBuffer[offset]->r)/255.0;
   pTexel->g = static_cast<double>(m_imageBuffer[offset]->g)/255.0;
   pTexel->b = static_cast<double>(m_imageBuffer[offset]->b)/255.0;
}

void Texture::Fit(double relativeX, double relativeY, drgbType* pTexel)
{
   //Get the pixel by multiplying by the x and y dimension
   GetTexel(static_cast<int>(relativeX*m_xDimension),
            static_cast<int>(relativeY*m_yDimension),
            pTexel);
}

void Texture::Tile(double worldX, double worldY, drgbType* pTexel)
{
   //The location in pixel coordinates
   int pixelX = static_cast<int>(worldX/m_pixelXSize);
   int pixelY = static_cast<int>(worldY/m_pixelYSize);

   //Tile modded by the dimension
   int texelX = pixelX%m_xDimension;
   int texelY = pixelY%m_yDimension;

   //Get the pixel
   GetTexel(texelX,texelY,pTexel);
}
