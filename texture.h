
#ifndef TEXTURE
#define TEXTURE

using namespace std;

class irgbType;
class drgbType;

class Texture
{
private:
   int m_xDimension;
   int m_yDimension;
   double m_pixelXSize;
   double m_pixelYSize;
   irgbType** m_imageBuffer;
   unsigned char* m_alphaValues;

   // The GetTexel function takes the x and y position of a pixel in the
   // texture and fills pTexel with that pixel.
   //    x - x coordinate.
   //    y - y coordinate.
   //    pTexel - Pointer to the pixel to store the pixel from the texture in.
   void GetTexel(int x, int y, drgbType* pTexel);
   void GetAlpha(int x, int y, double& alpha);

public:
   Texture(string textureFileName, double pixelXSize, double pixelYSize);
   ~Texture(void);

   // The Fit function fits a texture to a plane using the relative
   // coordinates of the pixel.
   //    relativeX - The relativeX coordinate of the pixel.
   //    relativeY - The relativeY coordinate of the pixel.
   //    pTexel - Pointer to a pixel to store the pixel from the texture in.
   void Fit(double relativeX, double relativeY, drgbType* pTexel);

   // The Tile function tiles a texture on a plane using the world x and y 
   // coordinates of the hit location.
   //    worldX - The world x location of the hit.
   //    worldY - The world y location of the hit.
   //    pTexel - Pointer to a pixel to store teh pixel from the texture in.
   void Tile(double worldX, double worldY, drgbType* pTexel);

   void GetAlphaFit(double relativeX, double relativeY, double& alpha);
   void GetAlphaTile(double worldX, double worldY, double& alpha);
};

#endif //TEXTURE
