
#ifndef RAYTRACER
#define RAYTRACER

#include <iostream>
#include <fstream>

using namespace std;

class Model;
class Vector;
class drgbType;
class Object;

class Raytracer
{
public:
   Raytracer(void);
   ~Raytracer(void);

   // The Raytrace function raytraces a model and writes the created image out
   // to the specified file.
   //    pModel - Pointer to the model to raytrace.
   //    outfile - Reference to the file to write the image to.
   void Raytrace(Model* pModel, ofstream& outfile);

private:
   //Max distance allowed for a ray to travel for specularity. A higher value
   //gives a better image, but takes longer to process.
   static const double m_CMaxDistance = 50;

   // The TraceRay function traces a single ray from the Base point in the 
   // direction Dir and stores the pixel value in Dpix. This function recurses
   // for specularity (reflection) and thus total distance is passed for the
   // recursion to put a limit on the total distance a ray can travel. See
   // m_CMaxDistance above.
   //    pModel - Pointer to the model to trace the ray in.
   //    pBase - Pointer to the vector containing the point that the ray
   //       originates from.
   //    pDir - Pointer to the vector containing the direction of the ray.
   //    pDpix - Pointer to the pixel to store the final color value in.
   //    totalDist - Total distance the ray has traveled thus far. Used to put
   //       an upper bound on the distance a ray can travel. This is to keep
   //       the raytracer from recursing to death.
   //    pLastHit - Pointer to the object that was last hit. This is used to
   //       keep the specular reflection from hitting itself again when it
   //       recurses.
   void TraceRay(Model* pModel,
                 Vector* pBase,
                 Vector* pDir,
                 drgbType* pDpix,
                 double totalDist,
                 Object* pLastHit);

   // The AddDiffuse function adds in the diffuse lighting by iterating through
   // the lights list and calling the illuminate function on each light.
   //    pModel - Pointer to the model to illuminate.
   //    pHitObject - Pointer to the object to illuminate.
   //    pPixel - Pointer to the pixel to add the diffuse to.
   void AddDiffuse(Model* pModel, Object* pHitObject, drgbType* pPixel);
};

#endif //RAYTRACER
