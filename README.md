#Raytracer

Thomas Langford

CSC4360

Version 1.2


This is an object-oriented raytracer.
The raytracer requires the [LibTiff](http://www.libtiff.org) package and g++.
A makefile is provided for building.

##Features
---
This raytracer can render the following:

* Planes
* Finite Planes
* Textured Planes
    * Tiled
    * Stretched
* Spheres
* Triangles
* Mirrored Surfaces
* Lights
* Obj files

##Compiling
---
To build Raytracer:

      make

To build with debug flags:

      make debug
      
If switching between a normal build and building with debug flags, the build 
directory should be cleaned before building.

To clean build directory:

      make clean

##Running
---
The Raytracer program can be run in three different ways.

1. Using IO redirection:

      ./Raytracer < scene > image.ppm

2. Using command line arguments:

      ./Raytracer scene image.ppm

3. Using the makefile:

      make run

This will run the raytracer program with the scene file "scene" and create the 
image "image.ppm". This default behavior can be changed by setting the `SCENE` 
variable for the run. This will run raytracer with the scene file "scene<SCENE>"
and output to the image "image<SCENE>.ppm."
For example the statement 

      make run SCENE=2
      
will run the raytracer with input from the file "scene2" and create the file 
"image2.ppm."

The default paths for locating the scene and image files are "scene/" and 
"image/" respectively. These can be changed by modifying the `SCENEDIR` and 
`IMAGEDIR` variables in the makefile.
