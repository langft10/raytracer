
#include <iostream>
#include <cmath>
#include <cstdlib>

#include "object.h"
#include "vector.h"
#include "material.h"
#include "texture.h"
#include "model.h"
#include "camera.h"

using namespace std;

Object::Object(void):
   m_pMaterial(NULL)
{
   m_pHitLoc = new Vector();
   m_pNormal = new Vector();
}

Object::Object(string type):
   m_type(type),
   m_pMaterial(NULL)
{
   m_pHitLoc = new Vector();
   m_pNormal = new Vector();
}

//Do not delete material in the object because the material pointers
//are stored in a list in the model. The model has ownership and will
//delete them
Object::~Object(void)
{
   delete m_pHitLoc;
   delete m_pNormal;
}

Vector* Object::GetHitLoc(void)
{
   return m_pHitLoc;
}

Vector* Object::GetNormal(void)
{
   return m_pNormal;
}

void Object::Print(void)
{
   cout << "Name: " << m_name << endl;
   cout << "Type: " << m_type << endl;
   cout << "Material: " << m_mat << endl;
   //Obj object will not have a material
   if(m_pMaterial != NULL) {
      m_pMaterial->Print();
   }
   cout << "Last Hit Location: " << *m_pHitLoc << endl;
   cout << "Normal: " << *m_pNormal << endl;
}

void Object::Parse(void)
{
   string token,junk;
   cin >> m_name;
   cin >> junk;
   cin >> token;

   if(token == "material")
      cin >> m_mat;
   else
      cerr << "Error: Material not first property in " << m_name << endl;
}

void Object::Init(Model* pModel)
{
   //Find the correct material and associate it with the object. Print an error
   //if it is not found.
   list<Material*>* pMaterials = &(pModel->m_mats);
   Material* tmp = NULL;
   for(list<Material*>::iterator it=pMaterials->begin(); it!=pMaterials->end(); it++) {
      if((*it)->GetName() == m_mat)
         tmp = *it;
   }
   if(tmp)
      m_pMaterial = tmp;
   else
      cerr << "Did not find material " << m_mat << " for " << m_name << endl;
}

string Object::GetMat(void)
{
   return m_mat;
}

void Object::GetAmbient(drgbType* pPixel)
{
   m_pMaterial->GetAmbient(pPixel);
}

void Object::GetDiffuse(drgbType* pPixel)
{
   m_pMaterial->GetDiffuse(pPixel);
}

void Object::GetSpecular(drgbType* pPixel)
{
   m_pMaterial->GetSpecular(pPixel);
}

string Object::GetName(void)
{
   return m_name;
}

void Object::SetMaterial(Material* ptr)
{
   m_pMaterial = ptr;
}

Plane::Plane(void):
   Object("plane")
{
}

Plane::~Plane(void)
{
   delete m_pNorToPlane;
   delete m_pPoint;
}

void Plane::Parse(void)
{
   Object::Parse();

   string token;
   double x,y,z;

   int i=0;
   do {
      cin >> token;
      if(token == "normal") {
         cin >> x >> y >> z;
         m_pNorToPlane = new Vector(x,y,z);
      }
      else if(token == "point") {
         cin >> x >> y >> z;
         m_pPoint = new Vector(x,y,z);
      }
   } while(++i<m_CMaxParseTokens);
   cin.ignore(100,'\n'); //Ignore everything after last token is checked
   if(static_cast<char>(cin.peek()) == '}') //Check for '}'
      cin >> token;
}

double Plane::Hits(Vector* pBase, Vector* pDirection)
{
   Vector thD;
   double norDotDir = *m_pNorToPlane * *pDirection;

   if(norDotDir == 0) {
#ifdef DEBUG
      cerr << "Plane::hits: Normal dot direction equal to zero" << endl;
#endif
      return -1;
   }

   double th = ((*m_pNorToPlane**m_pPoint) - (*m_pNorToPlane**pBase))/norDotDir;
   thD = th**pDirection;
   *m_pHitLoc = *pBase+thD;
   *m_pNormal = *m_pNorToPlane;
   return th;
}

void Plane::Print(void)
{
   Object::Print();

   cout << "Normal to Plane: " << *m_pNorToPlane << endl;
   cout << "Point: " << *m_pPoint << endl;
}

TiledPlane::TiledPlane(void):
   m_pAltMaterial(NULL),
   m_materialName("")
{
   m_type = "tiled_plane";
   for(int i=0; i<3; i++)
      rotation[i] = new Vector();
   m_pXdir = new Vector();
   m_pProjxdir = new Vector();
   m_pNewLoc = new Vector();
}

//Dont delete m_pAltMaterial because it's located in the
//material list
TiledPlane::~TiledPlane(void)
{
   delete m_pProjxdir;
   delete m_pXdir;
   delete m_pNewLoc;
   for(int i=0; i<3; i++)
      delete rotation[i];
}

void TiledPlane::Parse(void)
{
   Plane::Parse();

   string token;
   cin >> token;
   while(token != "}") {
      if(token == "xdir")
         cin >> *m_pXdir;
      else if(token == "altmaterial")
         cin >> m_materialName;
      else if(token == "dimension")
         cin >> m_dimensions[0] >> m_dimensions[1];
      cin >> token;
   }
}

void TiledPlane::Init(Model* pModel)
{
   Object::Init(pModel);

   //Find alternate material and associate it with the tiled plane
   Material* tmp = NULL;
   list<Material*>* pMaterials = &(pModel->m_mats);
   for(list<Material*>::iterator it=pMaterials->begin(); it!=pMaterials->end(); it++) {
      if((*it)->GetName() == m_materialName)
         tmp = *it;
   }
   if(tmp)
      m_pAltMaterial = tmp;
   else
      cerr << "Did not find material " << m_materialName << " for " << m_name << endl;

   Vector xDirUnit = m_pXdir->UnitVector();
   Vector normalUnit = m_pNorToPlane->UnitVector();
   *m_pProjxdir = xDirUnit.Projection(normalUnit);
   if(m_pProjxdir->x == 0.0 && m_pProjxdir->y == 0.0 && m_pProjxdir->z == 0.0) {
      cerr << "Error: Projxdir in finite plane is " << *m_pProjxdir << endl;
      exit(1);
   }
   *m_pProjxdir = m_pProjxdir->UnitVector();

   //Create rotation matrix:
   *(rotation[0]) = *m_pProjxdir;
   *(rotation[2]) = *m_pNorToPlane;
   *(rotation[2]) = (rotation[2])->UnitVector();
   *(rotation[1]) = rotation[2]->CrossProduct(*(rotation[0]));
}

int TiledPlane::SelectMaterial(void)
{
   //Apply rotation matrix so tiled plane is in xy plane
   Vector diff = *m_pHitLoc - *m_pPoint;
   m_pNewLoc->x = diff**(rotation[0]);
   m_pNewLoc->y = diff**(rotation[1]);
   m_pNewLoc->z = diff**(rotation[2]);

   //Determine the x and y index of the hit on the plane
   int xIndex = (100000+m_pNewLoc->x)/m_dimensions[0];
   int yIndex = (100000+m_pNewLoc->y)/m_dimensions[1];

   //Return the correct material based on even or odd index
   if((xIndex+yIndex)%2 == 0)
      return 1;
   return 0;
}

void TiledPlane::GetAmbient(drgbType* pPixel)
{
   if(SelectMaterial())
      m_pMaterial->GetAmbient(pPixel);
   else
      m_pAltMaterial->GetAmbient(pPixel);
}

void TiledPlane::GetDiffuse(drgbType* pPixel)
{
   if(SelectMaterial())
      m_pMaterial->GetDiffuse(pPixel);
   else
      m_pAltMaterial->GetDiffuse(pPixel);
}

void TiledPlane::GetSpecular(drgbType* pPixel)
{
   if(SelectMaterial())
      m_pMaterial->GetSpecular(pPixel);
   else
      m_pAltMaterial->GetSpecular(pPixel);
}

void TiledPlane::Print(void)
{
   Plane::Print();

   cout << "Dimensions: " << m_dimensions[0] << " " << m_dimensions[1] << endl;
   cout << "Alternate Material: " << m_materialName << endl;
   m_pAltMaterial->Print();
}

FinitePlane::FinitePlane(void)
{
   m_type = "finite_plane";
   for(int i=0; i<3; i++)
      rotation[i] = new Vector();
   m_pXdir = new Vector();
   m_pProjxdir = new Vector();
   m_pNewLoc = new Vector();
}

FinitePlane::~FinitePlane(void)
{
   delete m_pProjxdir;
   delete m_pXdir;
   delete m_pNewLoc;
   for(int i=0; i<3; i++)
      delete rotation[i];
}

void FinitePlane::Parse(void)
{
   Plane::Parse();
   string token;

   int i=0;
   do {
      cin >> token;
      if(token == "xdir")
         cin >> *m_pXdir;
      else if(token == "dimension")
         cin >> m_dimensions[0] >> m_dimensions[1];
   } while(++i<m_CMaxParseTokens);
   cin.ignore(100,'\n'); //Ignore everything after last token is checked
   if(static_cast<char>(cin.peek()) == '}') //Check for '}'
      cin >> token;
}

void FinitePlane::Init(Model* pModel)
{
   Object::Init(pModel);

   Vector xDirUnit = m_pXdir->UnitVector();
   Vector normalUnit = m_pNorToPlane->UnitVector();
   *m_pProjxdir = xDirUnit.Projection(normalUnit);
   if(m_pProjxdir->x == 0.0 && m_pProjxdir->y == 0.0 && m_pProjxdir->z == 0.0) {
      cerr << "Error: Projxdir in finite plane is " << *m_pProjxdir << endl;
      exit(1);
   }
   *m_pProjxdir = m_pProjxdir->UnitVector();
 
   //Create rotation matrix
   *(rotation[0]) = *m_pProjxdir;
   *(rotation[2]) = *m_pNorToPlane;
   *(rotation[2]) = (rotation[2])->UnitVector();
   *(rotation[1]) = rotation[2]->CrossProduct(*(rotation[0]));
}

double FinitePlane::Hits(Vector* pBase, Vector* pDirection)
{
   //Make sure the ray hits on the infinite plane before checking the finite
   //one
   double dist = Plane::Hits(pBase,pDirection);
   if(dist < 0.0)
      return dist;

   //Apply rotation matrix to rotate the finite plane into the xy plane
   Vector diff = *m_pHitLoc - *m_pPoint;
   m_pNewLoc->x = diff**(rotation[0]);
   m_pNewLoc->y = diff**(rotation[1]);
   m_pNewLoc->z = diff**(rotation[2]);

   //Check to see if the point falls within the dimensions
   if(m_pNewLoc->x >= 0.0 && m_pNewLoc->x <= m_dimensions[0] &&
      m_pNewLoc->y >= 0.0 && m_pNewLoc->y <= m_dimensions[1]) {
      return dist;
   }
   return -1;
}

void FinitePlane::Print(void)
{
   Plane::Print();

   cout << "Projected unititized x direction: " << *m_pProjxdir << endl;
   cout << "x direction: " << *m_pXdir << endl;
   cout << "Dimensions: " << m_dimensions[0] << " x " << m_dimensions[1] << endl;
   cout << "New hit location (rotated and translated): " << *m_pNewLoc << endl;
}

TexturedPlane::TexturedPlane(void):
   m_textureFileName(""),
   m_mode(0),
   m_pTexture(NULL)
{
   m_type = "textured_plane";
}

TexturedPlane::~TexturedPlane(void)
{
   delete m_pTexture;
}

void TexturedPlane::Parse(void)
{
   FinitePlane::Parse();
   string token;
   cin >> token;
   while(token != "}") {
      if(token == "texname") {
         cin >> m_textureFileName;
      } else if(token == "mode")
         cin >> m_mode;
      cin >> token;
   }
}

void TexturedPlane::Init(Model* pModel)
{
   FinitePlane::Init(pModel);

   double pixelXSize = pModel->m_pCam->GetWorldDim(0)/pModel->m_pCam->GetPixelDim(0);
   double pixelYSize = pModel->m_pCam->GetWorldDim(1)/pModel->m_pCam->GetPixelDim(1);
   m_pTexture = new Texture(m_textureFileName,pixelXSize,pixelYSize);
}

double TexturedPlane::Hits(Vector* pBase, Vector* pDirection)
{
   double dist = FinitePlane::Hits(pBase,pDirection);
   if(dist < 0.0)
      return dist;

   double alpha = 0.0;
   if(m_mode == 0)
      m_pTexture->GetAlphaFit(m_pNewLoc->x/m_dimensions[0],m_pNewLoc->y/m_dimensions[1],alpha);
   else if(m_mode == 1)
      m_pTexture->GetAlphaTile(m_pNewLoc->x,m_pNewLoc->y,alpha);

   if(alpha == 0.0)
      return -1.0;
   return dist;
}

void TexturedPlane::GetAmbient(drgbType* pPixel)
{
   m_pMaterial->GetAmbient(pPixel);
   drgbType texel;
   if(m_mode == 0)
      m_pTexture->Fit(m_pNewLoc->x/m_dimensions[0],m_pNewLoc->y/m_dimensions[1],&texel);
   else if(m_mode == 1)
      m_pTexture->Tile(m_pNewLoc->x,m_pNewLoc->y,&texel);
   *pPixel = *pPixel*texel;
}

void TexturedPlane::GetDiffuse(drgbType* pPixel)
{
   m_pMaterial->GetDiffuse(pPixel);
   drgbType texel;
   if(m_mode == 0)
      m_pTexture->Fit(m_pNewLoc->x/m_dimensions[0],m_pNewLoc->y/m_dimensions[1],&texel);
   else if(m_mode == 1)
      m_pTexture->Tile(m_pNewLoc->x,m_pNewLoc->y,&texel);
   *pPixel = *pPixel*texel;
}

void TexturedPlane::Print(void)
{
   FinitePlane::Print();

   cout << "Texture File: " << m_textureFileName << endl;
   cout << "Mode: " << m_mode << endl;
}

Sphere::Sphere(void):
   Object("sphere")
{
}

Sphere::~Sphere(void)
{
   delete m_pCenter;
}

void Sphere::Parse(void)
{
   Object::Parse();
   
   string token;
   double x,y,z;

   cin >> token;
   while(token != "}") {
      if(token == "radius") 
         cin >> m_radius;
      if(token == "center") {
         cin >> x >> y >> z;
         m_pCenter = new Vector(x,y,z);
      }
      cin >> token;
   }
}

double Sphere::Hits(Vector* pBase, Vector* pDirection)
{
   Vector diff;
   Vector vPrime;
   double a,b,c,th,radical;
   Vector thD;

   vPrime = *pBase - *m_pCenter;
   a = *pDirection**pDirection;
   b = 2*(vPrime**pDirection);
   c = vPrime*vPrime - m_radius*m_radius;
   radical = b*b-4*a*c;

   if(radical <= 0)
      return -1;

   diff = *pBase-*m_pCenter;
   if(diff.x < m_radius && diff.y < m_radius && diff.z < m_radius) {
      th = (-b + sqrt(radical))/(2*a);
   } else {
      th = (-b - sqrt(radical))/(2*a);
   }
   thD = th**pDirection;
   *m_pHitLoc = *pBase+thD;
   diff = *m_pHitLoc-*m_pCenter;
   *m_pNormal = diff.UnitVector();

#ifdef DEBUG
   cout << "Distance from sphere: " << th << endl;
#endif

   return th;
}

void Sphere::Print(void)
{
   Object::Print();

   cout << "Center: " << *m_pCenter << endl;
   cout << "Radius: " << m_radius << endl;
}

Triangle::Triangle(void):
   m_pPoint1(new Vector),
   m_pPoint2(new Vector),
   m_pPoint3(new Vector)
{
}

Triangle::Triangle(Vector* pPoint1, Vector* pPoint2, Vector* pPoint3):
   m_pPoint1(new Vector(*pPoint1)),
   m_pPoint2(new Vector(*pPoint2)),
   m_pPoint3(new Vector(*pPoint3))
{
   Vector BA = *pPoint2-*pPoint1;
   Vector CA = *pPoint3-*pPoint1;
   *m_pNormal = BA;
   m_pNormal->CrossProduct(CA);
}

Triangle::~Triangle(void)
{
   delete m_pPoint1;
   delete m_pPoint2;
   delete m_pPoint3;
}

void Triangle::Parse(void)
{
   Object::Parse();

   string token;

   cin >> token;
   while(token != "}") {
      if(token == "normal")
         cin >> *m_pNormal;
      else if(token == "pointA") 
         cin >> *m_pPoint1;
      else if(token == "pointB")
         cin >> *m_pPoint2;
      else if(token == "pointC")
         cin >> *m_pPoint3;
      cin >> token;
   }
}

double Triangle::Hits(Vector* pBase, Vector* pDirection)
{
   Vector edge1,edge2;
   Vector P,Q,T;
   double det; //Determinant
   double invDet; //Inverse determinant
   double u,v,t;

   edge1 = *m_pPoint2-*m_pPoint1;
   edge2 = *m_pPoint3-*m_pPoint1;

   P = pDirection->CrossProduct(edge2);
   det = P*edge1;

   //Check if the triangle is in the plane intersected by the ray
   if(det > -m_CEpsilon && det > m_CEpsilon)
      return -1;
   invDet = 1.0/det;

   T = *pBase-*m_pPoint1;

   u = (T*P)*invDet;
   if(u < 0.0 || u > 1.0)
      return -1.0;

   Q = T.CrossProduct(edge1);

   v = (*pDirection*Q)*invDet;
   if(v < 0.0 || u+v > 1.0)
      return -1.0;

   t = (edge2*Q)*invDet;

   if(t>m_CEpsilon) {
      *m_pHitLoc = *pBase+(*pDirection*t);
      return t;
   }
   return -1.0;
}

void Triangle::Print(void)
{
   Object::Print();

   cout << "Point A: " << *m_pPoint1 << endl;
   cout << "Point B: " << *m_pPoint2 << endl;
   cout << "Point C: " << *m_pPoint3 << endl;
}
