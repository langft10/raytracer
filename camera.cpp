
#include <iostream>
#include <fstream>
#include <cassert>

#include "camera.h"
#include "vector.h"
#include "rgb.h"

using namespace std;

Camera::Camera(void):
   m_pPixMap(NULL),
   m_pViewPoint(new Vector())
{
}

Camera::Camera(string newName,int px,int py,double wx,double wy,Vector* ptr):
   m_name(newName),
   m_pViewPoint(ptr)
{
   m_pixelDim[0] = px;
   m_pixelDim[1] = py;
   m_worldDim[0] = wx;
   m_worldDim[1] = wy;
}

Camera::~Camera(void)
{
   for(int i=0; i<m_pixelDim[0]*m_pixelDim[1]; i++)
      delete m_pPixMap[i];
   delete m_pPixMap;
}

void Camera::SetName(string newName) {
   m_name = newName;
}

void Camera::SetPixelDim(int x, int y) {
   m_pixelDim[0] = x;
   m_pixelDim[1] = y;
}

void Camera::SetWorldDim(int x, int y) {
   m_worldDim[0] = x;
   m_worldDim[1] = y;
}

void Camera::SetViewPoint(Vector* vec) {
   m_pViewPoint = vec;
}

int Camera::GetPixelDim(int x) {
   if(x != 0 && x != 1) {
      cerr << "Error: passing " << x << " to GetPixelDim" << endl;
   }
   return m_pixelDim[x];
}

double Camera::GetWorldDim(int x) {
   if(x != 0 && x != 1) {
      cerr << "Error: passing " << x << " to GetWorldDim" << endl;
   }
   return m_worldDim[x];
}

void Camera::Parse(void)
{
   string junk;
   double vx,vy,vz;
   vx = vy = vz = 0;
   Vector* ptr;

   cin >> m_name;
   cin.ignore();

   cin >> junk >> junk >> m_pixelDim[0] >> m_pixelDim[1];
   cin >> junk >> m_worldDim[0] >> m_worldDim[1];
   cin >> junk >> vx >> vy >> vz;
   cin >> junk;
   ptr = new Vector(vx,vy,vz);
   m_pViewPoint = ptr;
   m_pPixMap = new irgbType* [m_pixelDim[0]*m_pixelDim[1]];
}

void Camera::Print(void)
{
   cout << "Type: Camera" << endl;
   cout << "Name: " << m_name << endl;
   cout << "Pixel Dimensions: " << m_pixelDim[0] << " " << m_pixelDim[1] << endl;
   cout << "World Dimensions: " << m_worldDim[0] << " " << m_worldDim[1] << endl;
   cout << "View Point: " << *m_pViewPoint << endl;
   cout << endl;
   cout << "Image Dimension: " << m_pixelDim[0] << " " << m_pixelDim[1] << endl;
}

void Camera::CamDir(int x, int y, Vector* pRayDir)
{
   double wx = m_worldDim[0]*x/(m_pixelDim[0]-1);
   double wy = m_worldDim[1]*y/(m_pixelDim[1]-1);
   Vector P(wx,wy,0);
   *pRayDir = P-*m_pViewPoint;
   *pRayDir = pRayDir->UnitVector();
}

void Camera::SetPixel(int x, int y, drgbType* pPixel)
{
#ifdef DEBUG
   cout << "Setting pixel: " << x << " " << y << endl;
   cout << "   color: " << *pPixel << endl;
#endif
   int offset = (m_pixelDim[1]-y-1)*m_pixelDim[0]+x;
   m_pPixMap[offset] = new irgbType(pPixel);
#ifdef DEBUG
   irgbType* p=m_pPixMap[offset];
   cout << (int)p->r << " " << (int)p->g << " " << (int)p->b << endl;
#endif
}

void Camera::WriteImage(ofstream& outfile)
{
   //Required header of ppm image file
   outfile << "P6" << endl;
   outfile << "#RayTraced Image" << endl;
   outfile << m_pixelDim[0] << " " << m_pixelDim[1] << endl;
   outfile << "255" << endl;

   //Write each pixel
   for(int i=0; i<m_pixelDim[0]*m_pixelDim[1]; i++)
      outfile << *(m_pPixMap[i]);
}
