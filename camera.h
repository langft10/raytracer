
#ifndef CAMERA
#define CAMERA

#include <iostream>
#include <fstream>

using namespace std;

class irgbType;
class drgbType;
class Vector;
class Camera
{
private:
   string m_name;
   int m_pixelDim[2];
   double m_worldDim[2];
   irgbType** m_pPixMap;

public:
   Vector* m_pViewPoint; 
   Camera(void);
   // Constructor
   //    name - Name of the camera
   //    pixelDimX - Pixel x dimension of the image
   //    pixelDimY - Pixel y dimension of the image
   //    worldDimX - World x dimension of the image
   //    worldDimY - World y dimension of the image
   //    pViewPoint - Pointer to a vector containing the view point of the camera
   Camera(string name, int pixelDimX, int pixelDimY, double worldDimX, double worldDimY, Vector* pViewPoint);
   ~Camera(void);

   void SetName(string);
   void SetPixelDim(int,int);
   void SetWorldDim(int,int);
   void SetViewPoint(Vector*);

   int GetPixelDim(int);
   double GetWorldDim(int);

   // This function parses stdin for its name, pixel dimensions, world 
   // dimensions, and view point.
   void Parse(void);

   // This function prints information about this object to stdout, including
   // its name, pixel dimensions, world dimensions, and view point.
   void Print(void);

   // The CamDir function determines the unit vector in the direction of the
   // pixel at location (x,y) in the image from the camera's view point.
   //    x - x coordinate of the pixel in the image.
   //    y - y coordinate of the pixel in the image.
   //    pRayDir - Pointer to a vector to store the computed unit vector in.
   void CamDir(int,int,Vector*);

   // The SetPixel function sets a pixel at location (x,y) in the pixmap to pixel.
   //    x - x coordinate of the pixel in the image.
   //    y - y coordinate of the pixel in the image.
   //    pPixel - Pointer to the pixel to store in the pixmap.
   void SetPixel(int x, int y, drgbType* pPixel);

   // The WriteImage function writes the pixmap to outfile.
   //    outfile - The outfile to write the pixmap to.
   void WriteImage(ofstream& outfile);
};

#endif
