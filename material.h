
#ifndef MATERIAL
#define MATERIAL

#include "rgb.h"

using namespace std;

class Material
{
private:
   string m_name;
   drgbType m_ambient;
   drgbType m_diffuse;
   drgbType m_specular;

public:
   Material(void);
   Material(string name);
   ~Material(void);

   string GetName(void);
   void GetAmbient(drgbType* pPixel);
   void GetDiffuse(drgbType* pPixel);
   void GetSpecular(drgbType* pPixel);

   // The Parse function parses stdin for the material name, ambient, diffuse,
   // and specular values.
   void Parse(void);

   // The Print function prints information about the material including the
   // name, ambient, diffuse, and specular values.
   void Print(void);
};


#endif
