CC=g++
CFLAGS=-c -Wall
DFLAGS=-DDEBUG -g 
LIBRARIES=-ltiff
OUTFILE=RayTracer
SRC=main.cpp vector.cpp camera.cpp material.cpp object.cpp rgb.cpp light.cpp model.cpp raytracer.cpp parser.cpp texture.cpp obj.cpp
OBJ=$(SRC:.cpp=.o)
SCENEDIR=scenes/
IMAGEDIR=images/
SCENE= #Assign to SCENE which scene file to read from for full make and run
SCENEFILE=scene$(SCENE)
IMAGEFILE=image$(SCENE).ppm

all: main


main: $(OBJ)
	$(CC) $(OBJ) -o $(OUTFILE) $(LIBRARIES)

%.o: %.cpp %.h
	$(CC) $(CFLAGS) $< -o $@

debug: CFLAGS+=$(DFLAGS)
debug: main

run:
	./$(OUTFILE) $(SCENEDIR)$(SCENEFILE) $(IMAGEDIR)$(IMAGEFILE)
	eog $(IMAGEDIR)$(IMAGEFILE)

full: main run

clean:
	-rm *.o $(OUTFILE)

cleanall: clean
	-rm $(IMAGEDIR)/*.ppm

rebuild: clean main
