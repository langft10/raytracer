
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "model.h"
#include "raytracer.h"
#include "parser.h"

using namespace std;

int main(int argc, char** argv)
{
   //Redirect cin and cout based on if io redirection is being used or if
   //files are being passed in.
   ofstream outfile;
   ifstream infile;
   if(argc == 1) {
      cerr << "Reading from stdin." << endl <<
              "Outputting errors and logs to stderr." << endl <<
              "Outputting image to stdout." << endl;
      outfile.ios::rdbuf(cout.rdbuf()); //Reroute outfile to cout
      cout.rdbuf(cerr.rdbuf()); //Reroute cout to cerr
   } else if(argc == 3) {
      cout << "Reading from " << argv[1] << '.' << endl <<
              "Outputting logs to stdout." << endl <<
              "Outputting errors to stderr." << endl <<
              "Outputting image to " << argv[2] << '.' << endl;
      infile.open(argv[1],ifstream::in); //Open ifstream to input file
      cin.rdbuf(infile.rdbuf()); //Reroute cin to infile
      outfile.open(argv[2],ofstream::out); //Open ofstream to output file
   } else {
      cout << "Error: Invalid arguments." << endl <<
              "Usage: ./RayTracer < input > output.ppm" << endl <<
              "            OR" << endl << 
              "       ./Raytracer input output.ppm" << endl;
      exit(1);
   }

   Model* pModel = new Model();
   Parser parser;
   parser.Parse(pModel,infile);

   infile.close();
   pModel->Init();
   
   //Perform raytrace
   Raytracer tracer;
   tracer.Raytrace(pModel,outfile);

   outfile.close();

#ifdef DEBUG
   pModel->Print();
#endif

   delete pModel;
   return 0;
}
