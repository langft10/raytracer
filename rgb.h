
#ifndef RGBDATA
#define RGBDATA

#include <iostream>
using namespace std;

class drgbType;
class irgbType
{
public:
   unsigned char r;
   unsigned char g;
   unsigned char b;
   irgbType(void){};
   irgbType(unsigned char x,unsigned char y,unsigned char z):r(x),g(y),b(z){};
   irgbType(drgbType* dPixel);

   friend ostream& operator<<(ostream &, irgbType &);
   friend istream& operator>>(istream &, irgbType &);
};


class drgbType
{
public:
   double r;
   double g;
   double b;
   drgbType(void):r(0.0),g(0.0),b(0.0){};
   drgbType(double x, double y, double z):r(x),g(y),b(z){};

   friend ostream& operator<<(ostream &, drgbType &);

   const drgbType operator+(const drgbType&);
   const drgbType operator-(const drgbType&);
   const drgbType operator*(double);
   const drgbType operator*(const drgbType&);
   const drgbType operator/(double);
   drgbType& operator=(const drgbType&);
   bool operator<=(double);
   bool operator>(double);
   bool operator>=(double);
};

#endif
