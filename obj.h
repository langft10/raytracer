
#ifndef OBJ
#define OBJ

#include <vector>

#include "object.h"

using namespace std;

class Obj:public Object
{
protected:
   string m_objFile;
   string m_mtlFile;
   list<Triangle*> m_triangles;

   vector<Vector*> m_vertices;
   vector<Vector*> m_normals;
   vector<Vector*> m_textureCoords;

   Triangle* m_pLastHitTriangle;

   //The ParseObj function will open and parse an obj file with the filename
   //stored in m_objFile. It will populate the list of tirangles (m_triangles)
   //with triangle objects created with the verticies of the obj file.
   //   pModel - Pointer to the model that the material list can be found in
   void ParseObj(Model* pModel);

   //The ParseMtl function will open and parse a mtl file with the filename
   //stored in m_mtlFile.
   void ParseMtl(Model* pModel);

   //The ErrorAndExit function will log an error and terminate the program.
   //   fileName - The name of the file that the error is in.
   //   token - The token that the parser encountered an error with.
   void ErrorAndExit(string fileName, string token);

public:
   Obj(void);
   ~Obj(void);
   void Parse(void);
   void Init(Model* pModel);
   double Hits(Vector*, Vector*);
   void Print(void);
   void GetAmbient(drgbType* pPixel);
   void GetDiffuse(drgbType* pPixel);
   void GetSpecular(drgbType* pPixel);
};

#endif //OBJ
