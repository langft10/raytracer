
#include <iostream>

#include "parser.h"
#include "model.h"
#include "camera.h"
#include "material.h"
#include "object.h"
#include "light.h"
#include "obj.h"

using namespace std;

Parser::Parser(void)
{
   //Map each type to the function to parse its information
   m_ObjectTable["camera"] = &ParseCamera;
   m_ObjectTable["material"] = &ParseMaterial;
   m_ObjectTable["light"] = &ParseLight;
   m_ObjectTable["plane"] = &ParsePlane;
   m_ObjectTable["sphere"] = &ParseSphere;
   m_ObjectTable["tiled_plane"] = &ParseTiledPlane;
   m_ObjectTable["finite_plane"] = &ParseFinitePlane;
   m_ObjectTable["textured_plane"] = &ParseTexturedPlane;
   m_ObjectTable["triangle"] = &ParseTriangle;
   m_ObjectTable["obj"] = &ParseObj;
}

Parser::~Parser(void)
{
}

void Parser::Parse(Model* pModel, ifstream& infile)
{
   string token;
   while(cin >> token)
      m_ObjectTable[token](pModel);
}

void Parser::ParseCamera(Model* pModel)
{
   Camera* ptr = new Camera();
   if(ptr) {
      pModel->m_pCam = ptr;
      ptr->Parse();
   }
}

void Parser::ParseMaterial(Model* pModel)
{
   Material* ptr = new Material();
   if(ptr) {
      pModel->m_mats.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParsePlane(Model* pModel)
{
   Object* ptr = new Plane();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseTiledPlane(Model* pModel)
{
   Object* ptr = new TiledPlane();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseFinitePlane(Model* pModel)
{
   Object* ptr = new FinitePlane();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseTexturedPlane(Model* pModel)
{
   Object* ptr = new TexturedPlane();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseSphere(Model* pModel)
{
   Object* ptr = new Sphere();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseTriangle(Model* pModel)
{
   Object* ptr = new Triangle();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseObj(Model* pModel)
{
   Object* ptr = new Obj();
   if(ptr) {
      pModel->m_objs.push_back(ptr);
      ptr->Parse();
   }
}

void Parser::ParseLight(Model* pModel)
{
   Light* ptr = new Light();
   if(ptr) {
      pModel->m_lights.push_back(ptr);
      ptr->Parse();
   }
}
