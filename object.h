
#ifndef OBJECT
#define OBJECT

#include <iostream>
#include <cstring>
#include <list>

using namespace std;

class Vector;
class Material;
class Texture;
class drgbType;
class Model;

class Object
{
protected:
   string m_name;
   string m_type;
   string m_mat;
   Material* m_pMaterial;
   Vector* m_pHitLoc;
   Vector* m_pNormal;

public:
   Object(void);
   Object(string type);
   virtual ~Object(void);

   // The Parse function parses stdin for the name and material values. This
   // function is required by each inherited class because each class needs to
   // parse its own specific values.
   virtual void Parse(void)=0;

   // The Init function sets up the object before it is used. It should be
   // called before Hits is called. This is so the correct material pointer can
   // be associated with the object. Other computations and setup can be
   // performed in this class also, if it is overidden in inherited classes.
   //    pModel - Pointer to the model to obtain the list of materials.
   virtual void Init(Model* pModel);

   // The Print function prints out information about the object including the
   // name, type, and material information. This function can be overloaded in
   // inherited classes to print additional information about an object.
   virtual void Print(void);

   // The Hits function determines if the object was hit by a ray from point
   // Base in direction Direction. If so, it returns the distance to the hit
   // location
   //    pBase - Pointer to the vector containing the point that the Direction
   //       vector originates from.
   //    pDirection - Pointer to the vector containing the direction of the ray
   // This function returns the distance to the hit location.
   virtual double Hits(Vector* pBase, Vector* pDirection)=0;

   Vector* GetHitLoc(void);
   Vector* GetNormal(void);
   string GetMat(void);
   virtual void GetAmbient(drgbType* pPixel);
   virtual void GetDiffuse(drgbType* pPixel);
   virtual void GetSpecular(drgbType* pPixel);
   string GetName(void);
   void SetMaterial(Material*);
};


class Plane:public Object 
{
private:
   static const int m_CMaxParseTokens = 2;

protected:
   Vector* m_pNorToPlane;
   Vector* m_pPoint;

public:
   Plane(void);
   ~Plane(void);
   virtual void Parse(void);
   double Hits(Vector*, Vector*);
   void Print(void);
};

class TiledPlane:public Plane
{
private:
   Vector* rotation[3];
   Vector* m_pXdir;
   Vector* m_pProjxdir;
   double m_dimensions[2];
   Material* m_pAltMaterial;
   string m_materialName;

   int SelectMaterial(void);

protected:
   Vector* m_pNewLoc;

public:
   TiledPlane(void);
   ~TiledPlane(void);
   void Parse(void);
   void Init(Model* pModel);
   void GetAmbient(drgbType* pPixel);
   void GetDiffuse(drgbType* pPixel);
   void GetSpecular(drgbType* pPixel);
   void Print(void);
};

class FinitePlane:public Plane
{
private:
   Vector* rotation[3];
   Vector* m_pProjxdir;
   Vector* m_pXdir;
   static const int m_CMaxParseTokens = 2;

protected:
   Vector* m_pNewLoc;
   double m_dimensions[2];

public:
   FinitePlane(void);
   ~FinitePlane(void);
   void Parse(void);
   void Init(Model* pModel);
   double Hits(Vector*, Vector*);
   void Print(void);
};

class TexturedPlane:public FinitePlane
{
private:
   string m_textureFileName;
   int m_mode;
   Texture* m_pTexture;

public:
   TexturedPlane(void);
   ~TexturedPlane(void);
   void Parse(void);
   void Init(Model* pModel);
   double Hits(Vector*, Vector*);
   void GetAmbient(drgbType* pPixel);
   void GetDiffuse(drgbType* pPixel);
   void Print(void);
};

class Sphere:public Object 
{
protected:
   Vector* m_pCenter;
   double m_radius;

public:
   Sphere(void);
   ~Sphere(void);
   void Parse(void);
   double Hits(Vector*, Vector*);
   void Print(void);
};

class Triangle:public Object
{
protected:
   Vector* m_pPoint1;
   Vector* m_pPoint2;
   Vector* m_pPoint3;
   static const double m_CEpsilon = 0.000005;

public:
   Triangle(void);
   Triangle(Vector* pPoint1, Vector* pPoint2, Vector* pPoint3);
   ~Triangle(void);
   void Parse(void);

   //Find if ray intersects triangle using Moller-Trumbore algorithm
   double Hits(Vector*, Vector*);
   void Print(void);
};

#endif
