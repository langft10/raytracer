
#include <iostream>
#include <climits>

#include "light.h"
#include "vector.h"
#include "object.h"
#include "rgb.h"
#include "model.h"
#include "material.h"

using namespace std;

Light::Light(void):
   m_name("Light"),
   m_type("Light"),
   m_pLocation(NULL),
   m_pEmissivity(NULL)
{
}

Light::~Light(void)
{
   delete m_pLocation;
   delete m_pEmissivity;
}

void Light::Parse(void)
{
   string token,junk;
   double x,y,z;

   cin >> m_name;
   cin >> junk;

   cin >> token;
   while(token != "}") {
      if(token == "location") {
         cin >> x >> y >> z;
         m_pLocation = new Vector(x,y,z);
      }
      else if(token == "emissivity") {
         cin >> x >> y >> z;
         m_pEmissivity = new Vector(x,y,z);
      }
      cin >> token;
   }
}

void Light::Print(void)
{
   cout << "Name: " << m_name << endl;
   cout << "Type: " << m_type << endl;
   cout << "Location: " << *m_pLocation << endl;
   cout << "Emissivity: " << *m_pEmissivity << endl;
}

Vector* Light::GetLocation(void)
{
   return m_pLocation;
}

Vector* Light::GetEmissivity(void)
{
   return m_pEmissivity;
}

void Light::Illuminate(Model* pModel, Object* pHitObject, drgbType* pPixel)
{
   Vector direction; //Unit direction to light from hitPoint
   Object* closestObject = NULL; //Closest object in direction to light
   double close = 0.0; //Distance to closest object in direction to light
   double cos = 0.0; //cos of angle between normal and direction to light
   double dist = 0.0; //Distance to the light from the hitPoint
   drgbType diffuse(0.0,0.0,0.0);

   direction = *m_pLocation - *(pHitObject->GetHitLoc());
   dist = direction.Length();
   direction = direction.UnitVector();

   Vector normal = pHitObject->GetNormal()->UnitVector();
   cos = direction * normal;
   if(cos <= 0.0) //Object occluded by itself
      return;

   //See if there is an object inbetween the hit object and the light
   closestObject = pModel->FindClosestObject(pHitObject->GetHitLoc(),&direction,close,pHitObject);
   if(closestObject && close<dist) //Object occluded by a different object
      return;

   //Illuminate the object with diffuse lighting
   pHitObject->GetDiffuse(&diffuse);
   diffuse.r *= m_pEmissivity->x;
   diffuse.g *= m_pEmissivity->y;
   diffuse.b *= m_pEmissivity->z;
   diffuse = diffuse*(cos/dist);
   *pPixel = *pPixel+diffuse;
}
