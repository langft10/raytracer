
#ifndef MODEL
#define MODEL

#include <list>

using namespace std;

class Camera;
class Material;
class Object;
class Light;
class Vector;

class Model
{
public:
   Camera* m_pCam;
   list<Material*> m_mats;
   list<Object*> m_objs;
   list<Light*> m_lights;

   Model(void);
   ~Model(void);

   // The Print function iterates through the material, object, and light
   // lists and calls the Print function on each object.
   void Print(void);

   // The Init function initializes the model by iterating through each object
   // and calling the Init function on it.
   void Init(void);

   // The FindClosestObject function finds the closest object in the direction
   // Direction from the point Base and returns a pointer to it. This function
   // stores the distance to the closest object in the minDistance variable.
   // It does not consider the LastHit object in the search of the closest 
   // objects. Thus LastHit object will never be returned as the closest
   // object.
   //    pBase - Pointer to the base point that the direction vector originates
   //       from
   //    pDirection - Pointer to the vector containing the direction in which
   //       to search for the closest object.
   //    minDistance - Reference to variable to store the distance to the 
   //       closest object in.
   //    pLastHit - Pointer to the object that was last hit. This object will
   //       be excluded from the search. This parameter can be NULL, in which 
   //       case all objects will be considered.
   // This function returns a pointer to the closest object.
   Object* FindClosestObject(Vector* pBase, Vector* pDirection, double& minDistance, Object* pLastHit);
};

#endif
