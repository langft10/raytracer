
#ifndef PARSER
#define PARSER

#include <fstream>
#include <map>

using namespace std;

class Model;

class Parser
{
public:
   Parser(void);
   ~Parser(void);

   // The Parse function starts the parsing process by reading from stdin and
   // creating objects based off of the read type as specified in the 
   // object table.
   //    pModel - Pointer to the model to store the created objects in.
   //    infile - Reference to the file to read from
   void Parse(Model* pModel, ifstream& infile);

private:
   typedef void (*ParsingFunction)(Model*);
   map<string,ParsingFunction> m_ObjectTable;

   // Static functions to create each object and parse that object. Each
   // function takes a pointer to the model to store the created object in.
   static void ParseCamera(Model* pModel);
   static void ParseMaterial(Model* pModel);
   static void ParseLight(Model* pModel);
   static void ParsePlane(Model* pModel);
   static void ParseTiledPlane(Model* pModel);
   static void ParseFinitePlane(Model* pModel);
   static void ParseTexturedPlane(Model* pModel);
   static void ParseSphere(Model* pModel);
   static void ParseTriangle(Model* pModel);
   static void ParseObj(Model* pModel);
};

#endif //PARSER
