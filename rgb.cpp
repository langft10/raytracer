
#include <iostream>
using namespace std;

#include "rgb.h"

irgbType::irgbType(drgbType* pDPixel)
{
   *pDPixel = *pDPixel*255;
   if(pDPixel->r > 255)
      pDPixel->r = 255;
   if(pDPixel->g > 255)
      pDPixel->g = 255;
   if(pDPixel->b > 255)
      pDPixel->b = 255;
   if(pDPixel->r < 0)
      pDPixel->r = 0;
   if(pDPixel->g < 0)
      pDPixel->g = 0;
   if(pDPixel->b < 0)
      pDPixel->b = 0;
   r = static_cast<unsigned char>(pDPixel->r);
   g = static_cast<unsigned char>(pDPixel->g);
   b = static_cast<unsigned char>(pDPixel->b);
}

ostream& operator<<(ostream &outs, irgbType &c)
{
   outs << c.r << c.g << c.b;
   return outs;
}

istream& operator>>(istream &ins, irgbType &c)
{
   c.r = ins.get();
   c.g = ins.get();
   c.b = ins.get();
   return ins;
}

ostream& operator<<(ostream &outs, drgbType &c)
{
   outs << c.r << " " << c.g << " " << c.b;
   return outs;
}

const drgbType drgbType::operator+(const drgbType &other)
{
   drgbType rv = *this;
   rv.r = this->r + other.r;
   rv.g = this->g + other.g;
   rv.b = this->b + other.b;
   return rv;
}

const drgbType drgbType::operator-(const drgbType &other)
{
   drgbType rv = *this;
   rv.r = this->r - other.r;
   rv.g = this->g - other.g;
   rv.b = this->b - other.b;
   return rv;
}


const drgbType drgbType::operator*(double val)
{
   drgbType rv = *this;
   rv.r = this->r * val;
   rv.g = this->g * val;
   rv.b = this->b * val;
   return rv;
}

const drgbType drgbType::operator*(const drgbType &other)
{
   drgbType rv = *this;
   rv.r = this->r * other.r;
   rv.g = this->g * other.g;
   rv.b = this->b * other.b;
   return rv;
}

const drgbType drgbType::operator/(double val)
{
   drgbType rv = *this;
   rv.r = this->r / val;
   rv.g = this->g / val;
   rv.b = this->b / val;
   return rv;
}

drgbType& drgbType::operator=(const drgbType &other)
{
   if(this != &other) {
      this->r = other.r;
      this->g = other.g;
      this->b = other.b;
   }
   return *this;
}

bool drgbType::operator<=(double val)
{
   if(this->r <= val && this->g <= val && this->b <= val) 
      return true;
   return false;
}

bool drgbType::operator>(double val)
{
   if(this->r > val && this->g > val && this->b > val) 
      return true;
   return false;
}

bool drgbType::operator>=(double val)
{
   if(this->r >= val && this->g >= val && this->b >= val) 
      return true;
   return false;
}
