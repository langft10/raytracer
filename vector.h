#ifndef VECTOR
#define VECTOR

using namespace std;

class Vector
{
public:
   double x;
   double y;
   double z;

   Vector(void);
   Vector(double,double,double);
   Vector(const Vector&);

   const Vector operator+(const Vector&);
   const Vector operator-(const Vector&);
   const Vector operator*(double);
   friend const Vector operator*(double, const Vector&);
   double operator*(const Vector&);
   Vector& operator=(const Vector&);
   friend ostream& operator<<(ostream &, const Vector &);
   friend istream& operator>>(istream &, Vector &);
   double Length(void);
   const Vector UnitVector(void);
   const Vector Projection(const Vector&);
   const Vector CrossProduct(const Vector&);
};

#endif
