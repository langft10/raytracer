
#include <iostream>
#include <climits>

#include "raytracer.h"
#include "vector.h"
#include "model.h"
#include "rgb.h"
#include "object.h"
#include "material.h"
#include "light.h"
#include "camera.h"

using namespace std;

#ifdef DEBUG
int level = 0;
#endif

Raytracer::Raytracer(void)
{
}

Raytracer::~Raytracer(void)
{
}

void Raytracer::Raytrace(Model* pModel, ofstream& outfile)
{
   //Loop through each pixel in the image
   for(int y=0; y<pModel->m_pCam->GetPixelDim(1); y++) {
      for(int x=0; x<pModel->m_pCam->GetPixelDim(0); x++) {
//cout << "Processing Ray: " << y*pModel->m_pCam->GetPixelDim(1)+x << endl;
         Vector* pRayDir = new Vector();
         drgbType* pDColor = new drgbType(0.0,0.0,0.0);
         pModel->m_pCam->CamDir(x,y,pRayDir);
         TraceRay(pModel,pModel->m_pCam->m_pViewPoint,pRayDir,pDColor,0.0,NULL);
         pModel->m_pCam->SetPixel(x,y,pDColor);

         delete pRayDir;
         delete pDColor;
      }
   }
   pModel->m_pCam->WriteImage(outfile);
}

void Raytracer::TraceRay(Model* pModel,
                         Vector* pBase,
                         Vector* pDir,
                         drgbType* pDpix,
                         double totalDist,
                         Object* pLastHit)
{
#ifdef DEBUG
cout << "====================Level: " << level << endl;
#endif
   drgbType specular(0.0,0.0,0.0);
   double minDist = 0.0;
   drgbType thisRay(0.0,0.0,0.0);

   //Stop recursing if travelled the max distance
   if(totalDist > m_CMaxDistance)
      return;

   //Find the closest object and add the coloring from its ambient, diffuse
   //and specular values to the pixel
   Object* pClosest = pModel->FindClosestObject(pBase,pDir,minDist,pLastHit);
   if(pClosest) {
      totalDist += minDist;
      pClosest->GetAmbient(&thisRay); //Add ambient lighting
      AddDiffuse(pModel,pClosest,&thisRay); //Add diffuse lighting
#ifdef DEBUG
cout << "==================Total Distance: " << totalDist << endl;
cout << "minDist: " << minDist << endl;
#endif

      thisRay = thisRay/totalDist;

      //Add specular lighting and recurse to add reflection
      pClosest->GetSpecular(&specular);
      if(specular.r != 0.0 || specular.g != 0.0 || specular.b != 0.0) {
         drgbType specInt(0.0,0.0,0.0);
         Vector unitIn = (*pDir).UnitVector();
         Vector unitNorm = (*pClosest->GetNormal()).UnitVector();
         Vector negUnitIn = -1*unitIn;
         double uDotN = negUnitIn*unitNorm;
         Vector scaledNorm = (uDotN*2.0)*unitNorm;
         Vector reflectedDir = scaledNorm-negUnitIn;

#ifdef DEBUG
level++;
#endif
         TraceRay(pModel,pClosest->GetHitLoc(),&reflectedDir,&specInt,totalDist,pClosest);
#ifdef DEBUG
level--;
#endif

         specInt = specular*specInt;
         thisRay = thisRay+specInt;
      }
   }
   *pDpix = *pDpix+thisRay;
}

void Raytracer::AddDiffuse(Model* pModel, Object* pHitObject, drgbType* pPixel)
{
   for(list<Light*>::iterator it = pModel->m_lights.begin(); it!=pModel->m_lights.end(); it++)
      (*it)->Illuminate(pModel, pHitObject, pPixel);
}
