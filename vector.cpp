
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "vector.h"

using namespace std;

Vector::Vector(void):x(0.0),y(0.0),z(0.0){}
Vector::Vector(double a,double b,double c):x(a),y(b),z(c){}
Vector::Vector(const Vector& other):
   x(other.x),
   y(other.y),
   z(other.z)
{
}

const Vector Vector::operator*(double factor)
{
   Vector rv = *this;
   rv.x = this->x*factor;
   rv.y = this->y*factor;
   rv.z = this->z*factor;
   return rv;
}

const Vector operator*(double factor, const Vector& vec)
{
   Vector rv = vec;
   rv.x = vec.x*factor;
   rv.y = vec.y*factor;
   rv.z = vec.z*factor;
   return rv;
}

double Vector::operator*(const Vector& other)
{
   double x = this->x*other.x;
   double y = this->y*other.y;
   double z = this->z*other.z;
   return x+y+z;
}

const Vector Vector::operator-(const Vector& other)
{
   Vector rv = *this;
   rv.x = this->x-other.x;
   rv.y = this->y-other.y;
   rv.z = this->z-other.z;
   return rv;
}

const Vector Vector::operator+(const Vector& other)
{
   Vector rv = *this;
   rv.x = this->x+other.x;
   rv.y = this->y+other.y;
   rv.z = this->z+other.z;
   return rv;
}

Vector& Vector::operator=(const Vector& other)
{
   this->x = other.x;
   this->y = other.y;
   this->z = other.z;
   return *this;
}

ostream& operator<<(ostream &outs, const Vector &vector)
{
   outs << "<" << vector.x << "," << vector.y << "," << vector.z << ">";
   return outs;
}

istream& operator>>(istream &ins, Vector &vector)
{
   ins >> vector.x >> vector.y >> vector.z;
   return ins;
}

double Vector::Length(void)
{
   double a = x*x;
   double b = y*y;
   double c = z*z;
   return sqrt(a+b+c);
}

const Vector Vector::UnitVector(void)
{
   Vector rv = *this*(1.0/Length());
   return rv;
}

const Vector Vector::Projection(const Vector& PlaneNormal)
{
   Vector V = *this;
   double NDotV = V*PlaneNormal;
   Vector P = V-(NDotV*PlaneNormal);
   return P;
}

const Vector Vector::CrossProduct(const Vector& other)
{
   Vector Cross(this->y*other.z-this->z*other.y,
                this->z*other.x-this->x*other.z,
                this->x*other.y-this->y*other.x);
   return Cross;
}
